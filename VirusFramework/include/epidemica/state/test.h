
#if MPI_FOUND
#include <mpi.h>
#endif

namespace Epidemica
{
    class MPIBase
    {
    public:
#if MPI_FOUND
        MPI_Datatype GetMPIDatatype() const;
#endif
    };
}