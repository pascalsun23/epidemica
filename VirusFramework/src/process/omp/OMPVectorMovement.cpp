#if OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPVectorMovement.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <omp.h>

using namespace Epidemica;

Epidemica::OMP::OMPVectorMovement::OMPVectorMovement()
{
    m_generator = std::default_random_engine();
}

Epidemica::OMP::OMPVectorMovement::~OMPVectorMovement()
{
}

std::string Epidemica::OMP::OMPVectorMovement::GetName() const
{
    return "OMP::OMPVectorMovement";
}

void Epidemica::OMP::OMPVectorMovement::Execute(SimulationState& state)
{
    if (!state.IsDayTime()) return;

    float diffProbability = state.m_params.vectorDiffusionProbability;

    //TODO: SIER += and -= may need investigation
    #pragma omp parallel for
    for (int i = 0; i < (int)state.m_locations.size(); i++)
    {
        Location& location = state.m_locations[i];
        
        //get vector of src movers
        seir_vector srcMovers = seir_vector();
        for (int v = 0; v < INFECTION_STATES; v++)
        {
            std::binomial_distribution<int> binomial(location.m_vectorState.SEIR[v], diffProbability);
            srcMovers[v] = binomial(m_generator);

            //subtract from the src location
            location.m_vectorState.SEIR[v] -= srcMovers[v];
        }

        std::valarray<seir_vector> dstMoversArray = DistributeVectors(location, srcMovers);

        for (int neighbourId = 0; neighbourId < location.m_neighbours.size(); neighbourId++)
        {
            int dstId = location.m_neighbours[neighbourId];
            
            //Vector addition
            //state.m_locations[dstId].m_vectorState.SEIR += dstMoversArray[neighbourId];
            for (int j = 0; j < INFECTION_STATES; j++)
            {
                state.m_locations[dstId].m_vectorState.SEIR[j] += dstMoversArray[neighbourId][j];
            }
        }
    }
}

std::valarray<seir_vector> Epidemica::OMP::OMPVectorMovement::DistributeVectors(
        Location& location, 
        seir_vector srcMovers)
{
    auto dstMovers = std::valarray<seir_vector>(location.m_neighbours.size());

    //Use binomial distribution until movers is empty
    for (int i = 0; i < INFECTION_STATES - 1; i++)
    {
        int total = srcMovers[i];
        int moved = 0;
        for(int neighbourId = 0; neighbourId < location.m_neighbours.size(); neighbourId++)
        {
            float neighbourProbability = 1.0f / location.m_neighbours.size();
            if (moved == total) //dont move any more vectors
            {
                dstMovers[neighbourId][i] = 0;
            }
            else if (neighbourId == location.m_neighbours.size() - 1) //final group should receive remaining srcMovers
            {
                dstMovers[neighbourId][i] = total - moved;
                moved = total;
            }
            else //perform binomial sample
            {
                std::binomial_distribution<int> binomial(total, neighbourProbability);
                int sample = binomial(m_generator);
                dstMovers[neighbourId][i] = std::min(sample, total - moved);
                moved += sample;
            }
        }
#ifdef _DEBUG
        if (total != moved)
        {
            throw std::runtime_error("Did not move the correct amount at location");
        }
#endif
    }

    return dstMovers;
}
#endif