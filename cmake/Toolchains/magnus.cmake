

#This is the recommended environment to set for cray
#https://cmake.org/cmake/help/v3.5/manual/cmake-toolchains.7.html#cross-compiling-for-the-cray-linux-environment
set(CMAKE_SYSTEM_NAME CrayLinuxEnvironment)

set(CMAKE_C_COMPILER gcc)
set(CMAKE_CXX_COMPILER g++)

#These may not need to be set.
#set(CMAKE_VERSION_COMPILER_ENV_VAR todo)
#set(CMAKE_VERSION_COMPILER todo)
#set(CMAKE_0.1_COMPILER_ENV_VAR todo)
#set(MAKE_0.1_COMPILER todo)
#set(CMAKE_LANGUAGES_COMPILER_ENV_VAR todo)
#set(CMAKE_LANGUAGES_COMPILER todo)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")


#set(OpenMP_DIR path/to/OpenMPConfig.cmake)
#set(MPI_LIBRARY MPI_INCLUDE_PATH todo)
#set(OpenCL_DIR path/to/OpenCLConfig.cmake)

