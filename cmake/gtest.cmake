########################### 
# GTEST
##########################

# Enable ExternalProject CMake module
INCLUDE(ExternalProject)
SET_DIRECTORY_PROPERTIES(PROPERTIES EP_PREFIX ${CMAKE_BINARY_DIR}/third_party)

#=====================
# Google Test
#=====================
#project(googletest-download NONE)
ExternalProject_Add(
    googletest
    GIT_REPOSITORY 	  https://github.com/google/googletest.git
    GIT_TAG        	  master
	TIMEOUT           10
	
	# Force separate output paths for debug and release builds to allow easy
    # # identification of correct lib in subsequent TARGET_LINK_LIBRARIES commands
    # CMAKE_ARGS -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG:PATH=DebugLibs
    #            -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE:PATH=ReleaseLibs
    #            -Dgtest_force_shared_crt=ON
    
	# Disable install step
    LOG_DOWNLOAD ON
    LOG_CONFIGURE ON
    LOG_BUILD ON

	#DOWNLOAD_DIR "googletest-master/src" # The only dir option which is required
	#SOURCE_DIR        "${CMAKE_BINARY_DIR}/googletest-src"
	#BINARY_DIR        "${CMAKE_BINARY_DIR}/googletest-build"
	SOURCE_DIR         "${CMAKE_BINARY_DIR}/googletest"
	BINARY_DIR         "${PROJECT_BINARY_DIR}/bin"

	# Disable all other steps
	CONFIGURE_COMMAND ""
	BUILD_COMMAND     ""
	INSTALL_COMMAND   ""
	TEST_COMMAND      ""
)

ExternalProject_Get_Property(googletest binary_dir)
ExternalProject_Get_Property(googletest source_dir)
message("gtest src : ${source_dir}")
message("gtest bin : ${binary_dir}")


# Build gtest from existing sources
#ExternalProject_Add(
#    gtest
#    DOWNLOAD_COMMAND "" # No download required
#    SOURCE_DIR "googletest-master/src/googletest" # Use specific source dir
#    PREFIX "googletest-master" # But use prefix for compute other dirs
#    INSTALL_COMMAND ""
#    LOG_CONFIGURE ON
#    LOG_BUILD ON
#)

# gtest should be build after being downloaded
#add_dependencies(gtest googletest-master)

# Build gmock from existing sources
#ExternalProject_Add(
#    gmock
#    DOWNLOAD_COMMAND "" # No download required
#    SOURCE_DIR "googletest-master/src/googlemock" # Use specific source dir
#    PREFIX "googletest-master" # But use prefix for compute other dirs
#    INSTALL_COMMAND ""
#    LOG_CONFIGURE ON
#    LOG_BUILD ON
#)

# gmock should be build after being downloaded
#add_dependencies(gmock googletest-master)
	
#============================
# CMake Project Configuration
#============================
#ExternalProject_Get_Property(gmock source_dir)
#ExternalProject_Get_Property(gmock binary_dir)

#include_directories(${source_dir}/gtest/include)
#add_library(gtest STATIC IMPORTED)
#set_property(TARGET gtest PROPERTY IMPORTED_LOCATION ${binary_dir}/gtest/libgtest.a)
#add_dependencies(gtest project_gmock)
#add_library(gtest_main STATIC IMPORTED)
#set_property(TARGET gtest_main PROPERTY IMPORTED_LOCATION ${binary_dir}/gtest/libgtest_main.a)
#add_dependencies(gtest_main project_gmock)
#include_directories(${source_dir}/include)
#add_library(gmock STATIC IMPORTED)
#set_property(TARGET gmock PROPERTY IMPORTED_LOCATION ${binary_dir}/libgmock.a)
#add_dependencies(gmock project_gmock)
#add_library(gmock_main STATIC IMPORTED)
#set_property(TARGET gmock_main PROPERTY IMPORTED_LOCATION ${binary_dir}/libgmock_main.a)
#add_dependencies(gmock_main project_gmock)
	

# Specify include dir
#ExternalProject_Get_Property(googletest source_dir)
#set(GTEST_INCLUDE_DIR ${source_dir}/include)

# Library
#ExternalProject_Get_Property(googletest binary_dir)
#set(GTEST_LIBRARY_PATH ${binary_dir}/${CMAKE_FIND_LIBRARY_PREFIXES}gtest.a)
#set(GTEST_LIBRARY gtest)
#add_library(${GTEST_LIBRARY} UNKNOWN IMPORTED)
#set_property(TARGET ${GTEST_LIBRARY} PROPERTY IMPORTED_LOCATION
#                ${GTEST_LIBRARY_PATH} )
#add_dependencies(${GTEST_LIBRARY} googletest)