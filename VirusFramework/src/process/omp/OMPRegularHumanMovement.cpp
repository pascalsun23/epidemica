#if OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPRegularHumanMovement.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <omp.h>

Epidemica::OMP::OMPRegularHumanMovement::OMPRegularHumanMovement()
{
}

Epidemica::OMP::OMPRegularHumanMovement::~OMPRegularHumanMovement()
{
}

std::string Epidemica::OMP::OMPRegularHumanMovement::GetName() const
{
    return "OMP::OMPRegularHumanMovement";
}

void Epidemica::OMP::OMPRegularHumanMovement::Execute(SimulationState& state)
{
    //TODO: insert and erase may not be thread safe
    //#pragma omp parallel for
    for (int i = 0; i < state.m_hosts.size(); i++)
    {
        Host& host = state.m_hosts[i];

        int src = host.m_locationId;
        int dst = state.IsDayTime() ? host.m_hubId : host.m_homeId;
        if (host.m_hubId < 0) dst = host.m_homeId; //Can we do this without if?
        host.m_locationId = dst;
        state.m_locations[src].m_hosts.erase(i);
        state.m_locations[dst].m_hosts.insert(i);
    }
}
#endif