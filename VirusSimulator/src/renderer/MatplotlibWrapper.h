#pragma once

#include <epidemica/Types.h>

namespace Epidemica
{
    class TimeProfiler;

#ifdef MATPLOTLIBCPP_PYTHON_HEADER
    void DisplayEpidemicCurve(const std::vector<seir_vector>& hostsEpidemic);
#endif
    void DisplayProfiling(Epidemica::TimeProfiler& profiler);

}

