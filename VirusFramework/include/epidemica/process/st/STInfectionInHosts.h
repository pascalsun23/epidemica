#pragma once
#include <epidemica/process/IProcess.h>
#include <random>

namespace Epidemica
{
    namespace ST
    {
        /*
         * Progresses the infection state within each host.
         * (transmission to and from vectors is performed seperately)
         */
        class STInfectionInHosts : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            STInfectionInHosts();
            ~STInfectionInHosts();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
