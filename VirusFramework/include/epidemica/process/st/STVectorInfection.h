#pragma once
#include <epidemica/process/IProcess.h>

#include <random>
#include <type_traits>


#define DOUBLE_PRECISION
#ifdef DOUBLE_PRECISION
#define RealType double
#define REALTYPE(value) value
#else
#define RealType float
#define REALTYPE(value) value f
#endif

namespace Epidemica
{
    namespace ST
    {
        /*
         * Performs host to vector virus transmission.
         */
        class STVectorInfection : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            STVectorInfection();

            ~STVectorInfection();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

