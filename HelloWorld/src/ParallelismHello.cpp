#include "stdafx.h"
#include "ParallelismHello.h"

#include <iostream>
#include <sstream>
#include <omp.h>


#ifdef MPI_FOUND
#include <mpi.h>
#ifdef BOOST_MPI
#include <boost/mpi.hpp>
#include <boost/serialization/serialization.hpp>
//there also is a boost/numeric library that uses openmp and mpi 
//to solve ODE's, but they may have to be manually built

void ParallelismHello::OpenMP_MPI_Demo()
{
    boost::MPI::MPIenvironment env;
    boost::MPI::MPIcommunicator world;
    
    std::cout << "MPI world size : " << world.size() << std::endl;
    std::cout << "OMP max threads : " << (int)omp_get_max_threads() << std::endl;
    omp_set_num_threads(4);

    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        std::stringstream msg;
        msg << "Hello from : world " << world.rank() << " : thread " << tid << std::endl;
        std::cout << msg.str();
    }
}

#else

void ParallelismHello::OpenMP_MPI_Demo()
{
    MPI_Init(NULL, NULL);
    
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    
    std::cout << "MPI world size : " << world_size << std::endl;
    
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        std::stringstream msg;
        msg << "Hello from : world " << world_rank << " : thread " << tid << std::endl;
        std::cout << msg.str();
    }
    MPI_Finalize();
}

#endif
#endif