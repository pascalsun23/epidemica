#include "stdafx.h"
#include <iostream>
#include <tclap/CmdLine.h>

#include "Qt5WidgetsHello.h"

int main(int argc, char **argv)
{
    TCLAP::CmdLine cmd("Command description message", ' ', "v0.1");
    TCLAP::ValueArg<std::string> nameArg("s", "scenario", "Scenario to run", false, "test", "string", cmd);
    TCLAP::SwitchArg renderSwitch("r", "rendering", "Perform rendering of simulation", cmd, false);
    
    cmd.parse(argc, argv);
    
    std::string name = nameArg.getValue();
    bool rendering = renderSwitch.getValue();
    
    std::cout << "name : " << name << std::endl;
    std::cout << "rendering : " << rendering << std::endl;
    
    Epidemica::Qt5WidgetsHello::Run(argc, argv);
    
    return 0;
}
