cmake_minimum_required(VERSION 2.8.11)

set(TARGET_NAME VirusFrameworkTests)

file(
	GLOB_RECURSE CXX_SRCS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.cpp"
)

add_executable(${TARGET_NAME} ${CXX_SRCS})

#add GTest
add_dependencies(${TARGET_NAME} gtest_main gtest gmock_main gmock)
include_directories(${GTEST_INCLUDE_DIRS})
#include_directories("${gtest_SOURCE_DIR}/include")
set(LINK_OPTIONS "gtest;gmock;${LINK_OPTIONS}")
#set(LINK_OPTIONS "${GTEST_BOTH_LIBRARIES};${LINK_OPTIONS}")

#add VirusFramework
include_directories ("${CMAKE_CURRENT_LIST_DIR}/../VirusFramework/include")
set(LINK_OPTIONS VirusFramework;${LINK_OPTIONS})
target_link_libraries(${TARGET_NAME} gtest gtest_main)
target_link_libraries(${TARGET_NAME} ${LINK_OPTIONS})

#Setup Project
set_property(TARGET ${TARGET_NAME} PROPERTY FOLDER "tests")
set_target_properties(${TARGET_NAME} PROPERTIES
					  RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

# Add GTEST classes
### !Warning! make sure that the source files contain GTEST tests otherwise
### you may get a cmake regex error..
GTEST_ADD_TESTS(${TARGET_NAME} "" ${CXX_SRCS})

if(MSVC)
# Set Local Debugger Working Directory
file( WRITE "${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}.vcxproj.user"
"<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Project ToolsVersion=\"15.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">
	<PropertyGroup>
	<LocalDebuggerWorkingDirectory>$(OutputPath)</LocalDebuggerWorkingDirectory>
	<DebuggerFlavor>WindowsLocalDebugger</DebuggerFlavor>
	</PropertyGroup>
</Project>"
)
endif()