#if OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPInfectionInVectors.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <cmath>
#include <random>

#include <omp.h>

Epidemica::OMP::OMPInfectionInVectors::OMPInfectionInVectors()
{
    m_generator = random_engine();
}

Epidemica::OMP::OMPInfectionInVectors::~OMPInfectionInVectors()
{
}

std::string Epidemica::OMP::OMPInfectionInVectors::GetName() const
{
    return "OMP::OMPInfectionInVectors";
}

void Epidemica::OMP::OMPInfectionInVectors::Execute(SimulationState& state)
{
    double pTrans = 1.0 - std::exp(-state.m_params.vectorEIRate);

    unsigned int threadSeed = m_generator();

    #pragma omp parallel
    {
        //generator access is not thread safe, create one per thread.
        auto t_generator = random_engine(threadSeed + omp_get_thread_num());
        #pragma omp for
        for (int i = 0; i < (int)state.m_locations.size(); i++)
        {
            int ecount = state.m_locations[i].m_vectorState.SEIR[InfectionState::E];
            if (ecount == 0) continue;
            std::binomial_distribution<int> binomial(ecount, pTrans);
            int sample = binomial(t_generator);
            state.m_locations[i].m_vectorState.SEIR[InfectionState::E] -= sample;
            state.m_locations[i].m_vectorState.SEIR[InfectionState::I] += sample;
        }
    }
}
#endif