# Epidemica - Code Conventions

## Table of Contents

- [Epidemica - Code Conventions](#epidemica---code-conventions)
	- [Table of Contents](#table-of-contents)
	- [Intro](#intro)
	- [Code Structure Conventions](#code-structure-conventions)
		- [C/C++ Standard](#cc-standard)
		- [Headers and Forward Declarations](#headers-and-forward-declarations)
		- [Pointers and References](#pointers-and-references)
		- [Smart Pointers](#smart-pointers)
		- [Naming Conventions](#naming-conventions)
			- [File Names](#file-names)
			- [Class and Method Names](#class-and-method-names)
		- [Precompiled Headers](#precompiled-headers)
		- [Cross Platform Compliance](#cross-platform-compliance)
		- [Optional Libraries](#optional-libraries)
	- [Optimisation Conventions](#optimisation-conventions)
		- [Inlining](#inlining)
		- [Build Configurations](#build-configurations)
		- [Sequential Data Storage](#sequential-data-storage)

## Intro

This code conventions document is aimed to guide developers in making implementation and programming style decisions throughout the project lifecycle.

This project is not guarenteed to be completely spec compliant, and as such it is highly recommended to leave inline comment when intentionally going against agreed conventions.

## Code Structure Conventions

### C/C++ Standard

This code has currently been written to be compliant with **C/C++11**

Compilers that (nearly) fully support this standard include:

* gcc 4.9.2
* Visual C++ 2015 (MSVC 14.0)

As part of retaining productive development practices, developers should favour using C++ standard libraries for non high-performance sections of the code.
In situations where the standard library cannot provide suitable levels of utilities, consider using in order:

* a well optimised third party implementation
* standard c style code
* compiler specific libraries with compiler macro to help future developers identify where multiple implementations may be required.

Compiler specific macros checks currently in use include:

* #if WIN32
* #if __GNUC__

### Headers and Forward Declarations

Header files in this project should contain the extension `.h` when they declare classes, methods or functions but do not provide the implementation.

Header-only may use the `.hpp` extension to indicate that it is a header only library.

When writing header files, you should avoid including other header files where possible and instead use a forward declaration and then include other headers in the source implementation. This is in order to reduce compilation time.

Instances where it is appropriate to include another header inside a header are:

* When a class must inherit another class
* When a class stores another type by value
* When a function or method passes another type by value instead of reference.

### Pointers and References

As part of modern C/C++ practice, code should always pass by reference instead of pointers where possible. The advantages of doing this include:

* Less time spent refactoring with `*`, `->`, `&` notations
* makes it much harder to produce null pointer exceptions

When passing around references, use the `const` keyword whereever possible to protect the original programmer's intended usaged of a method.

### Smart Pointers

Smart pointers in C/C+= intelligently handle the disposing of heap memory when when the pointer goes out of scope.

The smart pointers that should be used in this project include:

* `std::unique_ptr`
* `std::shared_ptr`
* `std::weak_ptr`

In most cases for the non-high performance sections of the code, using a `std::shared_ptr` will be suitable as it is the most robust without out needing to consider object ownership.

When using `std::shared_ptr`:

* pass the smart pointer itself when giving the shared ownership to another class.
* pass the value of the smart pointer by reference when the value is not being stored by the method/function/constructor in question.

To initialize a smart pointer use the `.reset()` method

### Naming Conventions

#### File Names

All file containing object oriented code should match the class name held within them, followed by the `.cpp` or `.h` file extension.

Functional style code should generally be avoided in preference of static methods, or at least only be used in single source files.

Like classes, file names should be in UpeerCamelCase.

#### Class and Method Names

Classes and methods of this project should use UpperCamelCase

Pure virtual class names should begin with the `I` prefix

Abstract/virtual class names should use the `Abstract` or `A` prefix, or `Base` as the suffix

### Precompiled Headers

The precompiled header is a modern c++ compiler feature that greatly improves source file compilation performance when header-only libraries (such as all templated classes and standard Libraries)

The standard naming convention for this header is `pch/stdafx.h` in each compilable target, but can alternatively be named `pch/pch.h`

It is important that the precompiled header does not appear in any public `include` folders.

### Cross Platform Compliance

Use of standard and cross platform third party libraries in 99% of cases will typically result in cross platform compliant code.

Some important conventions however that will result in build errors include:

* "Linux systems uses case sensitive folders and filenames"
* "Linux systems require the use of `/` in string literals whereas windows allows `/` and `\\`
* "GCC requires that variable names DO NOT match their type/class"

Other conventions that will result in compiler warnings include:

* MSVC recommends using the `_s` version of C functions for safer error handling

Please update this section with common cross platform build failures as they appear.

### Optional Libraries

This project uses a number of cross platform third party libraries and high performance libraries that may not be available on all systems.

To help alleviate the challenges of complex environment setups for new users and systems, compiler macros have been defined for various libraries, some of which are completely optional to install and still allow the project to build.

## Optimisation Conventions

TODO

### Inlining

TODO

### Build Configurations

TODO

### Sequential Data Storage

TODO