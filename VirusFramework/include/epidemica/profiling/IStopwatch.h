#pragma once

namespace Epidemica
{
    class IStopwatch
    {
    public:
        //template<typename T>
        //virtual T GetTime() const = 0;

        virtual void Start() = 0;
        virtual void Stop() = 0;
        virtual void Reset() = 0;
        virtual void Restart() = 0;
    };
}