#pragma once
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    struct SimulationState;

    namespace ST
    {
        class STInfectionInVectors : public IProcess
        {
        private:
            random_engine m_generator;

        public:
            STInfectionInVectors();
            ~STInfectionInVectors();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

