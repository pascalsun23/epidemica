#pragma once
#include <string>
#include <map>
#include <functional>

#include "epidemica/process/ProcessPipeline.h"

namespace Epidemica
{
    class IProcess;
    typedef std::map<std::string, std::function<IProcess&()>> ProcessRegistry;
    
    
    /**
     * @class AbstractProcessFactory
     * @author Callan
     * @date 26/03/2017
     * @file AbstractProcessFactory.h
     * @brief Simulation executation can be treated as a sequence of pipeline operations, aka processes.
     * This means it is possible to make all processes inherit the proccess class, and an associated factory
     * for the implementation of the process.
     * In some cases, the available factories may be limited by the target platform at compile time or the execution runtime,
     * and as such, a fallback heirarchy may want to be explored. eg. CUDAFactory -> OpenCLFactory -> OpenMPFactory -> STFactory
     */
    class AbstractProcessFactory
    {
    private:
        std::map<std::string, std::function<IProcess&()>> registry;

    public:
        AbstractProcessFactory();
        ~AbstractProcessFactory();
        
        virtual void Initialize() {}
        virtual void Finalize() {}

        virtual ProcessPipeline* CreateProcessPipeline() { return new ProcessPipeline(); }
        virtual IProcess* CreateProcess(std::string processName) = 0;
        
        //TODO: consider returning null or error out instead of throwing and catching an exception
        virtual IProcess* CreateInfectionInVectors() = 0;
        virtual IProcess* CreateInfectionInHosts() = 0;
        virtual IProcess* CreateRegularHumanMovement() = 0;
        virtual IProcess* CreateVectorMovement() = 0;
        virtual IProcess* CreateVectorInfection() = 0;
        virtual IProcess* CreateHostInfection() = 0;
        virtual IProcess* CreateVectorPopulation() = 0;
        
        //TODO: add new processes here
        //virtual IProcess* CreateRandomHumanMovement() { }
	};
}