#pragma once
#ifdef OPENCL_FOUND

#include "epidemica/process/AbstractProcessFactory.h"

#include "CL/cl2.hpp"

namespace Epidemica
{
    class IProcess;
	class OpenCLProcessFactory : public AbstractProcessFactory
	{
    private:
        cl::Platform m_platform;
        cl::Device m_device;
        cl::Context m_context;
        
	public:
        
        cl::Device GetDevice() { return m_device; }
        cl::Context GetContext() { return m_context; }
    
		OpenCLProcessFactory();
		~OpenCLProcessFactory();

        static std::string tag() { return "OpenCL"; }
        
        IProcess* CreateProcess(std::string processName) override;
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}

#endif