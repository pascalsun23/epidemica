#pragma once

#include <epidemica/state/VectorState.h>
#include <epidemica/Types.h>
#include <ostream>
#include <sstream>

#if BOOST_FOUND
#include <boost/property_tree/ptree.hpp>
#endif
#if RAPIDJSON_FOUND
#include <rapidjson/document.h>
#endif
#if MPI_FOUND
#include <mpi.h>
#endif

namespace Epidemica
{
    /**
     * @brief class containing a single location node.
     * \author Callan Gray
     * \date 31/03/2017
     * \note
     */
    struct Location
    {
        int m_id;
        neighbour_vector m_neighbours;
        host_set m_hosts;

        float m_xcoord;
        float m_ycoord;
        VectorState m_vectorState;

        float XCoord() { return m_xcoord; }
        float YCoord() { return m_ycoord; }
        VectorState const& GetVectorState() { return m_vectorState; }

        neighbour_vector const& GetNeighbours() { return m_neighbours; }
        host_set const& GetHosts() { return m_hosts; }

        /**
         * @brief Default constructor.
         */
        Location() { }
        ~Location() { }

#if BOOST_FOUND
        Location(int id, const boost::property_tree::ptree& location);
#endif
#if RAPIDJSON_FOUND
        Location(int id, const rapidjson::GenericObject<true, rapidjson::Value>& location);
#endif

        /**
         * \bried Default constructor
         */
        Location(
            int id,
            float xcoord, 
            float ycoord, 
            int vectorCapacity,
            VectorState& vectorState,
            neighbour_vector& neighbours,
            host_set& hosts
        );

        /**
         * @brief Dynamically determines the maximum vector sizes
         * in order to generate a fixed size buffer for OpenCL kernels.
         */
        void* ToBuffer();

		friend std::ostream& operator<<(std::ostream& stm, const Location& a)
		{
			return stm << "Location(" 
				<< a.m_xcoord << ","
				<< a.m_ycoord << ")";
		}
			
#if MPI_FOUND
        static MPI_Datatype s_mpi_datatype;
        static void InitMPILayout();
#endif
    };
}