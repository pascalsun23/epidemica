#pragma once

#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
    class IProcess;
    
	class STProcessFactory : public AbstractProcessFactory
	{
	public:
		STProcessFactory();
		~STProcessFactory();

        static std::string tag() { return "ST"; }
        
        IProcess* CreateProcess(std::string processName) override;
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}

