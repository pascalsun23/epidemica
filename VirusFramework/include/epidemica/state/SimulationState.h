#pragma once

#include <epidemica/Types.h>
#include <epidemica/state/Params.h>
#include <epidemica/state/Location.h>
#include <epidemica/state/Host.h>
#include <epidemica/state/Config.h>

#include <vector>

namespace Epidemica
{
    struct Host;
    struct Location;
    class ISimulationScenario;
    class PTreeSimulationScenario;
    class JSONSimulationScenario;

    /**
     * @brief Storage class for all persistant data that is being processed by the simulation.
     *        Contains params, hosts, locations.
     *        This structure interleaves/groups data values in a way that should be optimal for
     *        proceess that iterates through hosts or locations
     */
    struct SimulationState
    {
    public:
        Config m_config;
        Params m_params; 
        vector<Host> m_hosts;
        vector<Location> m_locations;

        SimulationState();
        SimulationState(const ISimulationScenario& scenario);

        ~SimulationState();

        bool IsDayTime();
        seir_vector GetHostsEpidemic() const;

        friend std::ostream& operator<<(std::ostream & stm, const SimulationState& a);
    };
}