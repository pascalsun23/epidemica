#include "stdafx.h"
#include "epidemica/process/IProcess.h"

Epidemica::IProcess::IProcess()
{
}

#if BOOST_FOUND
Epidemica::IProcess::IProcess(boost::property_tree::ptree& properties)
{
}
#endif

Epidemica::IProcess::~IProcess()
{
}

std::ostream& Epidemica::operator<<(std::ostream& stm, const Epidemica::IProcess& a)
{
    stm << "IProcess: " << typeid(a).name();
    //a.operator<<(stm);
    return stm;
}