#include "stdafx.h"
#if MPI_FOUND
#include "epidemica/process/mpi/MPIProcessFactory.h"

#include "epidemica/process/st/STProcesses.h"
#include "epidemica/process/mpi/MPIProcesses.h"

#include <stdexcept>
#include <exception>

using namespace Epidemica;

Epidemica::MPIProcessFactory::MPIProcessFactory()
{
}

Epidemica::MPIProcessFactory::~MPIProcessFactory()
{
}

void Epidemica::MPIProcessFactory::Initialize()
{
    int argc = 0;
    char** argv = {};
    
    //TODO: potentially do MPI init here
    //MPI_Init(&argc, &argv);
    //MPI_Get()
    //std::cout << ""
}

//TODO: may want to introduce a fallback system instead
ProcessPipeline* Epidemica::MPIProcessFactory::CreateProcessPipeline()
{
    //TODO: make sure something is disposing these processes!
    std::vector<std::shared_ptr<IProcess>> processes = { std::shared_ptr<IProcess>(new ST::STVectorPopulation()) };
    return new ProcessPipeline(processes);
}

IProcess* Epidemica::MPIProcessFactory::CreateProcess(std::string processName)
{
    throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::MPIProcessFactory::CreateInfectionInVectors()
{
//#include "epidemica/process/st/STInfectionInVectors.h"
    return new Epidemica::MPI::MPIInfectionInVectors();
}

IProcess* Epidemica::MPIProcessFactory::CreateInfectionInHosts()
{
    throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::MPIProcessFactory::CreateRegularHumanMovement()
{
    throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::MPIProcessFactory::CreateVectorMovement()
{
    throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::MPIProcessFactory::CreateVectorInfection()
{
    throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::MPIProcessFactory::CreateHostInfection()
{
    throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::MPIProcessFactory::CreateVectorPopulation()
{
    throw std::runtime_error("Not implemented");
}

void Epidemica::MPIProcessFactory::Finalize()
{
    //MPI_Finalize();
}
#endif