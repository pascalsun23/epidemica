#pragma once

#include "QApplication"
#include "QWidget"
#include "QDebug"
#include "cmath"

namespace Epidemica
{
    class Qt5WidgetsHello : public QWidget
    {
    public:
        /***
         * Check out this: http://blog.debao.me/2013/08/how-to-use-qthread-in-the-right-way-part-1/
         **/
        static int Run(int argc, char **argv);
    };
}