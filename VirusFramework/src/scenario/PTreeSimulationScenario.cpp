#if BOOST_FOUND
#include "stdafx.h"
#include "epidemica/scenario/PTreeSimulationScenario.h"

#include <boost/optional/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem/path.hpp>

#if __GNUC__
#include <sys/stat.h>
#endif

namespace pt = boost::property_tree;
namespace fs = boost::filesystem;

inline bool FileExists(const std::string& filepath)
{
    //TODO: ifstream implementation on magnus has issues
    //use posix functions in the meantime
#if __GNUC__
    struct stat buffer;
    return (stat(filepath.c_str(), &buffer) == 0);
#else
    std::ifstream file = std::ifstream(filepath);
    return file.good();
#endif
}


Epidemica::PTreeSimulationScenario::PTreeSimulationScenario()
{
}

Epidemica::PTreeSimulationScenario::PTreeSimulationScenario(std::string filename)
{
    LoadScenario(filename);
}

Epidemica::PTreeSimulationScenario::~PTreeSimulationScenario()
{
}

void Epidemica::PTreeSimulationScenario::LoadState(SimulationState& state) const
{
    //Config
    state.CurrentCycle = m_config.get<int>("current-cycle");
    state.m_totalCycles = m_config.get<int>("total-cycles");

    //Params
    state.m_params = Params(m_params);

    //Locations
    state.m_locations = vector<Location>(m_locations.size());
    int i = 0;
    for (auto it = m_locations.begin(); it != m_locations.end(); it++)
    {
        int id = atoi(it->first.c_str());
        state.m_locations[i] = Location(id, it->second);
        i++;
    }

    //Hosts
    state.m_hosts = vector<Host>(m_hosts.size());
    i = 0;
    for (auto it = m_hosts.begin(); it != m_hosts.end(); it++)
    {
        state.m_hosts[i] = Host(i, it->second);
        i++;
    }
}

void Epidemica::PTreeSimulationScenario::SaveState(const SimulationState & state)
{
}

void Epidemica::PTreeSimulationScenario::SaveStateCheckpoint(const SimulationState & state)
{
}

void Epidemica::PTreeSimulationScenario::LoadScenario(const std::string& configpath)
{
    //CONFIG
    m_configfile = configpath;
    fs::path directory = fs::path(m_configfile).parent_path();

    try
    {
        pt::read_json(m_configfile, m_config);
    }
    catch (pt::json_parser_error const& ex)
    {
        std::cerr << "failed to read : " << m_configfile << std::endl;
        std::cerr << ex.message() << std::endl;
        throw ex;
    }

    //PARAMS FILE
    try
    {
        auto paramsfile = m_config.get_optional<std::string>("params-file");

        if (paramsfile) m_paramsfile = paramsfile.get();
        else m_paramsfile = "params.json";

        if (!FileExists(m_paramsfile))
        {
            //try relative path to config file location
            m_paramsfile = directory.string() + "/" + m_paramsfile;
        }

        pt::read_json(m_paramsfile, m_params);
    }
    catch (pt::json_parser_error const& ex)
    {
        std::cerr << "failed to read : " << m_paramsfile << std::endl;
        std::cerr << ex.message() << std::endl;
        throw ex;
    }

    //HOSTSFILE
    try
    {
        boost::optional<std::string> hostsfile = m_config.get_optional<std::string>("hosts-file");

        //TODO: maybe pick a better default based on filename
        if (hostsfile) m_hostsfile = hostsfile.get();
        else m_hostsfile = "hosts.json";

        if (!FileExists(m_hostsfile))
        {
            //try relative path to config file location
            m_hostsfile = directory.string() + "/" + m_hostsfile;
        }

        pt::read_json(m_hostsfile, m_hosts);
    }
    catch (pt::json_parser_error const& ex)
    {
        std::cerr << "failed to read : " << m_hostsfile << std::endl;
        std::cerr << ex.message() << std::endl;
        throw ex;
    }

    //LOCATIONSFILE
    try
    {
        boost::optional<std::string> locationsfile = m_config.get_optional<std::string>("locations-file");
        if (locationsfile) m_locationsfile = locationsfile.get();
        else m_locationsfile = "locations.json";

        if (!FileExists(m_locationsfile))
        {
            //try relative path to config file location
            m_locationsfile = directory.string() + "/" + m_locationsfile;
        }

        pt::read_json(m_locationsfile, m_locations);
    }
    catch (pt::json_parser_error const& ex)
    {
        std::cerr << "failed to read : " << m_locationsfile << std::endl;
        std::cerr << ex.message() << std::endl;
        throw ex;
    }
}

void Epidemica::PTreeSimulationScenario::SaveScenario(const std::string& configpath)
{
    m_configfile = configpath;

    try
    {
        //CONFIG
        pt::write_json(m_configfile, m_config);

        //PARAMS FILE
        pt::write_json(m_paramsfile, m_params);

        //HOSTS FILE
        pt::write_json(m_hostsfile, m_hosts);

        //LOCAtIONS FILE
        pt::write_json(m_locationsfile, m_locations);
    }
    catch (pt::json_parser_error const& ex)
    {
        std::cerr << "failed to write to file" << std::endl;
        std::cerr << ex.message() << std::endl;
        throw ex;
    }
}

#endif //BOOST_FOUND