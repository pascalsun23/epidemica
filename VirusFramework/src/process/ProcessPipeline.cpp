#include "stdafx.h"
#include "epidemica/process/AbstractProcessFactory.h"
#include "epidemica/process/IProcess.h"
#include "epidemica/process/ProcessPipeline.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/profiling/TimeProfiler.h"
#include "epidemica/profiling/Stopwatch.h"

#include <easylogging++.h>

Epidemica::ProcessPipeline::ProcessPipeline()
{

}

Epidemica::ProcessPipeline::ProcessPipeline(const std::vector<std::shared_ptr<IProcess>>& processes) :
    m_processes(processes)
{
}

Epidemica::ProcessPipeline::~ProcessPipeline()
{
}

void Epidemica::ProcessPipeline::Execute(Epidemica::SimulationState& state, Epidemica::TimeProfiler* profiler)
{
    if(profiler == nullptr)
    {
        for (auto it = m_processes.begin(); it != m_processes.end(); it++)
        {
            (*it)->Execute(state);
        }
    }
    else
    {
        Epidemica::Stopwatch stopwatch;
        for (auto it = m_processes.begin(); it != m_processes.end(); it++)
        {
            stopwatch.Restart();
            (*it)->Execute(state);

#if _DEBUG
            //TODO: run checks here to confirm which process causes bugs
            for (int i = 0; i < state.m_locations.size(); i++)
            {
                Location& location = state.m_locations[i];
                int total = 0;
                for (int j = 0; j < SEIR_LENGTH; j++)
                {
                    total += location.m_vectorState.SEIR[j];
                }
                if (total > location.m_vectorState.Capacity + 100)
                {
                    LOG(WARNING) << "location vector total is greater than capacity by 100 after " << (*it)->GetName();
                    //throw std::runtime_error("location vector total is greater than capacity by " <<  << " after " << (*it)->GetName();
                }
            }
#endif

            stopwatch.Stop();
            double time = stopwatch.GetTime<double>();
            profiler->AddTime((*it)->GetName(), time);
        }
    }

    //TODO, this increment is a little out of place, but its not worth
    //creating a seperate process for it.
    state.m_config.CurrentCycle += 1;
    LOG(TRACE) << "cycle : " << state.m_config.CurrentCycle;

}

std::ostream& Epidemica::operator<<(std::ostream& os, const Epidemica::ProcessPipeline& a)
{
    os << "ProcessPipeline:";
    for (auto it = a.m_processes.cbegin(); it != a.m_processes.cend(); it++)
    {
        os << std::endl << **it;
    }
    return os;
}
