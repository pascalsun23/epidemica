/*! \mainpage Arbovirus Simulator
*	\section intro_sec Introduction
*	This is the arbovirus simulator, the versable, customizable virus simulator.\n
*	This Document outlines every class, namespace, and file structure, and you can choose from the top menu.
*
*	\section sim_prop Simulation Properties
*	The purpose of this simulator is to simulate an arbovirus transmission model,
*	using SEIR model, with a combination of process. \n
*	In particular, processes include:
*		- Regular Human Movement
*		- Vector Movement
*		- Host to Vector Infection
*		- Vector to Host Infection
*		- Vector Population process  
*	
*	For any requirement, such as making human born/death effect, weather effect,
*	you are welcome to do so.
*
*	\section rel_file Relative Pages
*	The relative pages section contains all pages we created using markdown,
*	including a general usage guide, description of some key folders.
*
*	\section name_sp Namespaces
*	In C++, NameSpace is where we classify programs, which is the key for us to have same function name from
*	different implementation.\n
*	Section Namespaces at the top of this page will provide you a total view of namespace,
*	where clicking each of them will shows available classes,
*	enabling you to view available algorithms in each implementations.
*
*	\section class Classes
*	Classes section lists all available classes (which exculded third-party libraries), with description
*	of each classses, enabling you to understand the purpose of each class.\n
*	You can choose multiple organizing strategy, if you like.
*
*	\section file Files
*	File section provides a folder view of all involved C++ source code/header files in this project.\n
*	Location of exactly file are non compulsory, but we strongly suggest when doing further maintainance
*	to follow origin file structure, to make it easier to find file, together avoid linking problems.
*	\section Maintain Guides
*	A guideline for any developer, if you want to, to maintain the simulator, in particular, any user who wants to:
*		- Add in any simulation process, to perform differnet vector / human population process  
*		- Add in weather effects  
*		- Remove certain process  
*	
*	Please refer to [the maintainance guideline](@ref virusMaintainGuide)
*	\author Simon
*	\date 31 October 2017
*/