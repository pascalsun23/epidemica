#include "stdafx.h"
#include "Qt5WidgetsHello.h"


int Epidemica::Qt5WidgetsHello::Run(int argc, char **argv)
{
    QApplication app(argc, argv);
    
    Qt5WidgetsHello widget;
    
    widget.resize(640,480);
    widget.show();
    
    return app.exec();
}