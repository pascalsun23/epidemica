#include "stdafx.h"

#include <epidemica/DeviceManager.h>
#include <epidemica/Simulation.h>
#include <epidemica/state/SimulationState.h>
#include <epidemica/process/IProcess.h>
#include <epidemica/process/ProcessPipeline.h>
#include <epidemica/scenario/ScenarioFactory.h>

#include <exception>
#include <chrono>
#include <ctime>

#if MPI_FOUND
#include <mpi.h>
#endif

#include "ProgramOptions.h"
#include "helper.h"

#include <easylogging++.h>

#if PROGRAM_OPTIONS == PROGRAM_OPTIONS_BOOST
namespace po = boost::program_options;
#endif

using namespace Epidemica;

INITIALIZE_EASYLOGGINGPP
int main(int argc, char **argv)
{
    //TODO: this has to either go here at the very start, and possibly
    //only do file loading on the primary node, and memory allocation on all nodes?
    //If input files are avaialable on all nodes, we can put this call here
    //and be done with it.
#if MPI_FOUND
    MPI_Init(&argc, &argv);
    int id;
    int p;
    MPI_Comm_size ( MPI_COMM_WORLD, &p ); 
    MPI_Comm_rank ( MPI_COMM_WORLD, &id ); 
#endif

    try
    {
#if PROGRAM_OPTIONS == PROGRAM_OPTIONS_CXXOPTS
        cxxopts::Options options("Epidemica", "Virus Simulatior");
        ParseArguments(argc, argv, options);

        if (options.count("help"))
        {
            std::cout << options.help() << std::endl;
            return 0;
        }

#elif PROGRAM_OPTIONS == PROGRAM_OPTIONS_BOOST
        po::options_description desc{ "Options" };
        po::variables_map options;
        ParseArguments(argc, argv, desc, vm);

        if (options.count("help"))
        {
            std::cout << options << std::endl;
            return 0;
        }
#endif
        //=======================
        // Loading
        //=======================
        //By default, load whatever is written in taskconfig.json
        std::string scenarioFile = "taskconfig.json"; 
        if (options.count("scenario"))
        {
            scenarioFile = options["scenario"].as<std::string>();
        }

        std::shared_ptr<DeviceManager> deviceManager = std::make_shared<DeviceManager>();

        std::shared_ptr<ISimulationScenario> scenario = std::shared_ptr<ISimulationScenario>(
            ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(scenarioFile));

        std::cout << "Running " << scenario->GetFileLocation() << std::endl;

        std::shared_ptr<Simulation> simulation = std::shared_ptr<Simulation>(
            LoadSimulation(options, deviceManager, scenario));


        //===================
        //Logging
        //===================
        el::Configurations conf("log.conf");
        el::Loggers::reconfigureAllLoggers(conf);
#if MPI_FOUND
        // mpirun -np 4 ./VirusSimulation 
        std::stringstream logFilePath;
        logFilePath << "node" << id << ".log";
        el::Loggers::reconfigureAllLoggers(el::ConfigurationType::Filename, logFilePath.str());
#endif
        std::time_t cur_time = std::time(nullptr);
        LOG(INFO) << "Start at:" << std::asctime(std::localtime(&cur_time));
        LOG(INFO) << "eventType, cycle, host_ID, cell_ID";



        RunSimulation(*simulation);

        ProcessResults(options, *simulation);
    }
    catch(std::exception const& ex)
    {
        std::cerr << ex.what() << std::endl;
        std::cout << "Press any key to exit...";
        std::cin.get();
    }

    std::time_t cur_time = std::time(nullptr);
    LOG(INFO) << "Finish at:" << std::asctime(std::localtime(&cur_time));
#if MPI_FOUND
    MPI_Finalize();
#endif

    return 0;
}
