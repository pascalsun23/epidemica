#include "MatplotlibWrapper.h"

#include <map>
#include <string>
#include <iostream>
#include <epidemica/profiling/TimeProfiler.h>


#ifdef MATPLOTLIBCPP_PYTHON_HEADER
//WARNING: these headers include static definitions, can only be
//included once in the entire solution!
//Wrap methods properly here for now, could possible replace this
//the library in future with a better one.


//Windows python install doesn't include debug python libs.
//This is the current accepted way of including it atm.
#if _DEBUG
	#undef _DEBUG
	#include <Python.h>
	#include <matplotlib-cpp/matplotlibcpp.h>
	#define _DEBUG
#else
	#include <Python.h>
	#include <matplotlib-cpp/matplotlibcpp.h>
#endif


void Epidemica::DisplayEpidemicCurve(const std::vector<seir_vector>& hostsEpidemic)
{
        //swizzle data
        std::vector<int> susceptable, exposed, infected, recovered = std::vector<int>();
        for (seir_vector v : hostsEpidemic)
        {
            susceptable.push_back(v[0]);
            exposed.push_back(v[1]);
            infected.push_back(v[2]);
            recovered.push_back(v[3]);
        }

        //TODO: There is a bug in the library which cause a crash if
        //named_plot gets called before plot, may be in python or C++ :(

		//Try running MatPlot, and see if error occur.
		try
		{
			matplotlibcpp::plot({});
			matplotlibcpp::named_plot("susceptable", susceptable);
			matplotlibcpp::named_plot("exposed", exposed);
			matplotlibcpp::named_plot("infected", infected);
			matplotlibcpp::named_plot("recovered", recovered);
			matplotlibcpp::legend();
			matplotlibcpp::show();
		}
		catch (const std::runtime_error& error)
		{
			std::cout << "Seems there is a problem on loading related packages." << std::endl;
			std::cout << "Please make sure you have all related Python packages installed." << std::endl;
			std::cout << "Checkout readme file, \"Python Matplot Library (Optional)\" section for more information." << std::endl;
		}
}
#endif

void Epidemica::DisplayProfiling(Epidemica::TimeProfiler& profiler)
{

    std::map<std::string, double>& metrics = *profiler.GetMetrics();

    //matplotlibcpp::plot({});
    for (auto cit = metrics.cbegin(); cit != metrics.cend(); ++cit)
    {
        std::cout << "process: " << cit->first << " time: " << cit->second / profiler.GetTotalTime() << std::endl;
        //matplotlibcpp::named_plot(cit->first, cit->second);
    }
}
