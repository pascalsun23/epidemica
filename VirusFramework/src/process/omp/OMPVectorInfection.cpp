#if OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPVectorInfection.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <omp.h>

Epidemica::OMP::OMPVectorInfection::OMPVectorInfection()
{
    m_generator = std::default_random_engine();
}

Epidemica::OMP::OMPVectorInfection::~OMPVectorInfection()
{
}

std::string Epidemica::OMP::OMPVectorInfection::GetName() const
{
    return "OMP::OMPVectorInfection";
}

void Epidemica::OMP::OMPVectorInfection::Execute(SimulationState& state)
{
    double bitingRate = state.m_params.bitingRate;
    double infectionProbability = state.m_params.vectorInfectionProbability;

    #pragma omp parallel for
    for (int locationId = 0; locationId < (int)state.m_locations.size(); locationId++)
    {
        Location& location = state.m_locations[locationId];
        int susceptible = location.m_vectorState.SEIR[InfectionState::S];

        if (susceptible == 0) continue;

        int hosts = 0;
        int infectedHosts = 0;

        //consider doing this aggregation elsewhere
        for(auto& hostId : location.m_hosts)
        {
            hosts += 1;
            if (state.m_hosts[hostId].m_infectionState == InfectionState::I) infectedHosts += 1;
        }

        if (infectedHosts == 0) continue;

        //rate is proportional to the number hosts already infected
        double beta = (double)(infectedHosts / hosts) * bitingRate * infectionProbability;
        double p = 1.0f - std::exp(-beta);

        std::binomial_distribution<int> binomial(susceptible, p);
        int newInfections = binomial(m_generator);

        location.m_vectorState.SEIR[InfectionState::S] -= newInfections;
        location.m_vectorState.SEIR[InfectionState::E] += newInfections;
    }
}
#endif