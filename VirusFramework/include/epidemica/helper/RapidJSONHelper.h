#pragma once
#if RAPIDJSON_FOUND

#include <epidemica/Types.h>


//TODO: shouldn't need to include these here for
// vector template argument. Try forward declaring
// with templates.
#include <epidemica/state/Host.h>
#include <epidemica/state/Location.h>

#include <string>
#include <exception>
#include <stdexcept>

#include <rapidjson/document.h>

namespace Epidemica
{
    struct Host;
    struct Location;

    /**
     * Helper functions for using the RapidJSON library.
     */
    class RapidJSONHelper
    {
    public:
        /**
         * Parse from a rapidjson generic object of a given key to a template value.
         * @exception std::invalid_argument Thrown when an invalid argument error condition occurs.
         * @tparam T Generic type parameter.
         * @param object The object.
         * @param key    The key.
         * @return A T.
         */
        template<typename T>
        static T ParseValue(const rapidjson::GenericObject<true, rapidjson::Value>& object, const char* key)
        {
#if _DEBUG
            if (!object.HasMember(key))
            {
                throw std::invalid_argument("member does not exist: " + std::string(key));
            }
            if (!object[key].Is<T>())
            {
                throw std::invalid_argument("member not of type expected type: " + std::string(typeid(T).name()));
            }
#endif
            return object[key].Get<T>();
        }

        /**
         * Parse value or default
         * @tparam T Generic type parameter.
         * @param object       The object.
         * @param key          The key.
         * @param defaultValue The default value.
         * @return the value.
         */
        template<typename T>
        static T ParseValueOrDefault(const rapidjson::GenericObject<true, rapidjson::Value>& object, const char* key, T defaultValue)
        {
#if _DEBUG
            if (object.HasMember(key) && object[key].Is<T>())
            {
                return object[key].Get<T>();
            }
            else
            {
                //LOG(INFO) << "Using default value for: " << key << std::endl;
                return defaultValue;
            }
#else
            if (object.HasMember(key) && object[key].Is<T>())
            {
                return object[key].Get<T>();
            }
            else
            {
                return defaultValue;
            }
#endif
        }

        /**
         * Serialize value with a useful exception in Debug mode.
         * @exception std::invalid_argument Thrown when an invalid argument error condition occurs.
         * @tparam T Generic type parameter.
         * @param object The object.
         * @param key    The key.
         * @param value  The value.
         *
         * ### tparam T Generic type parameter.
         */
        template<typename T>
        static void SerializeValue(const rapidjson::GenericObject<false, rapidjson::Value>& object, const char* key, T value)
        {
#if _DEBUG
            if (!object.HasMember(key))
            {
                std::stringstream ss;
                ss << "error serializing value of " << value << ", key : "  << key << " not present";
                throw std::invalid_argument(ss.str());
            }

            if (!object[key].Is<T>())
            {
                std::stringstream ss;
                ss << "error serializing value of " << value << ", object is not of type : " << typename(value);
                throw std::invalid_argument(ss.str());
            }
#endif
            object[key].Set(value);
        }

        /**
         * Safely serialize a value at the cost of checking type safetly.
         * @param          object    The object.
         * @param          key       The key.
         * @param          value     The value.
         * @param [in,out] allocator The allocator.
         *
         * ### tparam T Generic type parameter.
         */
        template<typename T>
        static void SafeSerializeValue(
            const rapidjson::GenericObject<false, rapidjson::Value>& object, 
            const char* key, 
            T value,
            rapidjson::MemoryPoolAllocator<>& allocator)
        {
            if (!object.HasMember(key))
            {
                rapidjson::GenericStringRef<char> keyString(key, (rapidjson::SizeType)strlen(key));

                object.AddMember<int>(keyString, value, allocator);
            }

            else if (!object[key].Is<T>())
            {
                rapidjson::GenericStringRef<char> keyString(key, (rapidjson::SizeType)strlen(key));
                object.EraseMember(keyString);
                object.AddMember<T>(keyString, value, allocator);
            }
            else
            {
                object[key].Set(value);
            }
        }


        /**
         * Saves the location state to the rapidjson scenario object. NOTE: this should get implemented
         * as a method of SimulationState and to a class called LocationCollection in future for flexibility.
         * @param          locations The locations.
         * @param          params    Options for controlling the operation.
         * @param [in,out] allocator The allocator.
         */
        static void SaveLocations(
            const vector<Location>& locations,
            const rapidjson::GenericObject<false, rapidjson::Value>& params,
            rapidjson::MemoryPoolAllocator<>& allocator);

        /**
         * Saves the host state to the rapidjson scenario object. NOTE: this should get implemented as a
         * method of SimulationState and to a class called LocationCollection in future flexibility.
         * @param          hosts     The hosts.
         * @param          params    Options for controlling the operation.
         * @param [in,out] allocator The allocator.
         */
        static void SaveHosts(
            const vector<Host>& hosts,
            const rapidjson::GenericObject<false, rapidjson::Value>& params,
            rapidjson::MemoryPoolAllocator<>& allocator);
    };
}

#endif
