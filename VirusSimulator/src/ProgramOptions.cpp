
#include "ProgramOptions.h"

#if BOOST_FOUND
#include <boost/program_options.hpp>
namespace po = boost::program_options;
void ParseArguments(int argc, char** argv, po::options_description& desc, po::variables_map& vm)
{
    desc.add_options()
        // <name,short_name> | <description>  
        ("help,h", "Help screen")
        //("interactive", "Interactive Mode")
        ("epicurve,e", "Draw Epidemic Curve")
        ("cycles,c", "Number of Cycles")
        ("printstats,s", "Print epidemic curve values")
        ("profiling,p", "Display profiled times of processes");

    desc.add_options()
        // <name,short_name> | <type> | <description>  
        ("scenario,s", po::value<std::string>()->default_value("test3new/test3.config.json"), "Scenario File")
        ("implementation,i", po::value<std::string>()->default_value("ST"), "Implementation Tag")
        //("seed", po::value<int>())
        ;

    //store all program arguments in a variable map
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
}
#endif

#if CXXOPTS_FOUND
void ParseArguments(int argc, char** argv, cxxopts::Options& options)
{
    options.add_options()
        ("h,help", "Help screen")
        //("interactive", "Interactive Mode")
        ("e,epicurve", "Draw Epidemic Curve")
        ("c,cycles", "Number of Cycles")
        ("printstats", "Print epidemic curve values")
        ("p,profiling", "Display profiled times of processes");


    options.add_options()
        // <name,short_name> | <type> | <description>  
        ("s,scenario", "Scenario File", cxxopts::value<std::string>()->default_value("taskconfig.json"))
        ("i,implementation", "Implementation Tag", cxxopts::value<std::string>()->default_value("ST"))
        ("seed", "Generator Seed", cxxopts::value<int>())
        ;

    options.parse(argc, argv);
}
#endif