#pragma once
#if OPENMP_FOUND
#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
    class IProcess;
	class OpenMPProcessFactory : public AbstractProcessFactory
	{
	public:
        /**
         * Default constructor
         */
        OpenMPProcessFactory();

        /**
         * Destructor
         */
        ~OpenMPProcessFactory();

        /**
         * Gets the tag
         * @return A std::string.
         */
        static std::string tag() { return "OMP"; }

        /**
         * Creates the process from a name
         * [not implemented]
         * @param processName Name of the process.
         * @return Null if it fails, else the new process.
         */
        IProcess* CreateProcess(std::string processName) override;

        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}

#endif