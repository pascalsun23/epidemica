# VirusControlPanel

This project is for a GUI program for a user to perform configuration and analysis on the simulation.

This program should include:
* User friendly GUI window
* Scenario Tool
	* Scenario list
	* Scenario editor
	* Process selection
* Analytics Tool
	* Report list
* Run Tool
	* Scenario list
	* Remote deploy option
	* Execution settings:
		* MPI support
		* OpenMP support
		* OpenCL Support
		* Track performance
		* Track data every 1/10/100/1000 cycles
		* Other tracking settings
	* Enable Window Mode
		* Show realtime trackers
		* Show realtime visualisation
		* Limit framerate
	