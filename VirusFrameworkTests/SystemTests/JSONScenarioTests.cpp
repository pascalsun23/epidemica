
#include <epidemica/scenario/JSONSimulationScenario.h>
#include <epidemica/scenario/ScenarioFactory.h>
#include <epidemica/Simulation.h>
#include <epidemica/DeviceManager.h>
#include <epidemica/state/SimulationState.h>
#include <memory>

#include <gtest/gtest.h>

namespace Epidemica
{
    void TestScenario(const std::string& filename)
    {
        std::unique_ptr<ISimulationScenario> scenario =
            std::unique_ptr<ISimulationScenario>(
                ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));
    }

    void TestScenarioSaving(const std::string filename)
    {
        std::unique_ptr<ISimulationScenario> scenario =
            std::unique_ptr<ISimulationScenario>(
                ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

        scenario->SaveScenarioToFile("testsave", "");

        std::unique_ptr<ISimulationScenario> savedScenario =
            std::unique_ptr<ISimulationScenario>(
                ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

        JSONSimulationScenario& jsonScenario = static_cast<JSONSimulationScenario&>(*scenario);
        JSONSimulationScenario& jsonSavedScenario = static_cast<JSONSimulationScenario&>(*savedScenario);

        ASSERT_EQ(jsonScenario.GetParams(), jsonSavedScenario.GetParams());
        ASSERT_EQ(jsonScenario.GetHosts(), jsonSavedScenario.GetHosts());
        ASSERT_EQ(jsonScenario.GetLocations(), jsonSavedScenario.GetLocations());
        ASSERT_TRUE(jsonScenario.Compare(jsonSavedScenario));
    }

    TEST(ScenarioTests, test2x2)
    {
        TestScenario("scenarios/test2x2/config.json");
    }

    TEST(ScenarioTests, test2x2_legacy)
    {
        TestScenario("scenarios/test2x2legacy/params.json");
    }

    TEST(ScenarioTests, test5x4)
    {
        TestScenario("scenarios/test5x4/test.config.json");
    }

    TEST(ScenarioTests, test5x4_legacy)
    {
        TestScenario("scenarios/test5x4legacy/test.params.json");
    }

    TEST(ScenarioTests, testDefaultConfig)
    {
        TestScenario("taskconfig.json");
    }

    TEST(ScenarioTests, test2x2Save)
    {
        TestScenarioSaving("scenarios/test2x2/config.json");
    }

    //TEST_F
    //TEST_P
    //TYPED_TEST
    //TYPED_TEST_CASE
    //TYPED_TEST_P
    //TYPED_TEST_CASE_P

    //INSTANTIATE_TEST_CASE_P(AllScenarios, DramTest, testing::Values(1, 2, 3));
}