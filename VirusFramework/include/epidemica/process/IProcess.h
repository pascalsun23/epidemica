#pragma once

namespace Epidemica
{
    struct SimulationState;


    /**
    * @class IProcess
    * @author Callan
    * @date 26/03/2017
    * @file IProcess.h
    * @brief All operations that alter a simulation state should implement this.
    */
    class IProcess
    {
    public:
        IProcess();

#if BOOST_FOUND
        #include <boost/property_tree/ptree.hpp>
        IProcess(boost::property_tree::ptree& properties);
#endif

        ~IProcess();

        /**
         * @brief Gets the name of the process.
         * @return The name.
         */
        virtual std::string GetName() const = 0;

        /**
         * @brief Executes the process on a given state.
         * @param [in,out] state The state.
         */
        virtual void Execute(SimulationState& state) = 0;
    };
	
	std::ostream& operator<<(std::ostream& stm, const IProcess& a);
}