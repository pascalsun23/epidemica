#pragma once
#if RAPIDJSON_FOUND
#include <epidemica/scenario/ISimulationScenario.h>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/filereadstream.h>

#include <string>
#include <memory>

namespace Epidemica
{
    /**
     * A JSON simulation scenario.
     */
    class JSONSimulationScenario : public ISimulationScenario
    {
    private:
        std::string m_configfile;
        std::shared_ptr<rapidjson::Document> m_config;

        std::string m_paramsfile;
        std::shared_ptr<rapidjson::Document> m_params;

        std::string m_hostsfile;
        std::shared_ptr<rapidjson::Document> m_hosts;

        std::string m_locationsfile;
        std::shared_ptr<rapidjson::Document> m_locations;

    public:
        const rapidjson::Document& GetConfig() const;
        const rapidjson::Document& GetHosts() const;
        const rapidjson::Document& GetLocations() const;
        const rapidjson::Document& GetParams() const;

        /**
         * Default constructor
         */
        JSONSimulationScenario();

        /**
         * Constructor
         * @param filename Filename of the file.
         */
        JSONSimulationScenario(const std::string& filename);

        const std::string& GetFileLocation() override;

        /**
         * @brief Loads scenario from file
         */
        void LoadScenarioFromFile() override;

        /**
         * @brief Loads the scenario from a config file
         * @param configpath The configpath.
         */
        void LoadScenarioFromFile(const std::string& configpath) override;


        /**
         * @brief Saves the scenario to the currently active file location.
         */
        void SaveScenarioToInputFile() override;

        /**
         * @brief Saves the scenario to a specified location, but keep the active file location.
         * (Use this for backup saving, e.g. checkpoints)
         * @param configpath    The configpath.
         * @param paramspath    The paramspath.
         * @param hostspath     The hostspath.
         * @param locationspath The locationspath.
         */
        void SaveScenarioToFile(
            const std::string& configpath,
            const std::string& paramspath,
            const std::string& hostspath,
            const std::string& locationspath);

        /**
         * @brief Saves a scenario to a specified filepath.
         *        TODO: this currently does not create the folder if it does not exist.
         * @param name The name.
         * @param path Full pathname of the file.
         */
        void SaveScenarioToFile(const std::string& name, const std::string& path) override;

        /**
         * @brief Saves the scenario to checkpoint file
         */
        void SaveScenarioToCheckpointFile() override;


        /**
         * Loads a RapidJSON document at a given filepath
         * @param          filepath The filepath.
         * @param [in,out] doc      The document.
         */
        void LoadDocument(const std::string& filepath, rapidjson::Document& doc);

        /**
         * Loads a RapidJSON document from a given filepath in order of:
         * - file relative to working directory.
         * - directory with file relative to working directory.
         * 
         * 
         * @param          key         filename key in the config document.
         * @param          defaultname The defaultname.
         * @param          directory   Pathname of the directory.
         * @param [in,out] file        The file.
         * @param [in,out] doc         The document.
         */
        void LoadDocumentFallback(
            const char* key,
            const char* defaultname,
            const std::string& directory,
            std::string& file,
            rapidjson::Document& doc);

        /**
         * Saves a document
         * @param filepath The filepath.
         * @param doc      The document.
         */
        void SaveDocument(const std::string& filepath, const rapidjson::Document& doc);

        /**
         * @brief Loads the state from this scenario object
         * @param [in,out] state The state.
         */
        void LoadState(SimulationState& state) const override;

        /**
         * @brief Saves the state into this scenario object
         * @param state The state.
         */
        void SaveState(const SimulationState& state) override;

        /**
         * Compares this const JSONSimulationScenario&amp; object to another to determine their relative
         * ordering
         * @param other Another instance to compare.
         * @return True if it succeeds, false if it fails.
         */
        bool Compare(const JSONSimulationScenario& other) const;

      private:


        void UpdateConfigPaths(
            const std::string& paramspath,
            const std::string& hostspath,
            const std::string& locationspath);

        /**
        * @brief Converts a scenario filepath to a checkpoint filepath
        * @return The checkpoint filepath.
        */
        std::string AppendCheckpointStamp(const tm& time, const std::string& filepath);
    };
}
#endif