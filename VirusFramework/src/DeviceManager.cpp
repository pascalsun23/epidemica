#include "stdafx.h"
#include "DeviceManager.h"
#include "epidemica/process/AbstractProcessFactory.h"
#include "epidemica/process/ProcessPipeline.h"

#include "epidemica/process/st/STProcessFactory.h"
#include "epidemica/process/opencl/OpenCLProcessFactory.h"
#include "epidemica/process/omp/OMPProcessFactory.h"
#include "epidemica/process/mpi/MPIProcessFactory.h"
#include <epidemica/Globals.h>

using namespace Epidemica;

#if BOOST_FOUND
namespace pt = boost::property_tree;
#endif

Epidemica::DeviceManager::DeviceManager()
{
    m_processFactories = ProcessFactoryMap();
    m_processFactories.insert(std::make_pair(
        STProcessFactory::tag(), 
        std::unique_ptr<AbstractProcessFactory>(new STProcessFactory())));
#ifdef OPENMP_FOUND
    m_processFactories.insert(std::make_pair(
        OpenMPProcessFactory::tag(), 
        std::unique_ptr<AbstractProcessFactory>(new OpenMPProcessFactory())));
#endif
#ifdef MPI_FOUND
    m_processFactories.insert(std::make_pair(
        MPIProcessFactory::tag(),
        std::unique_ptr<AbstractProcessFactory>(new MPIProcessFactory())));
#endif
#if OPENMP_FOUND && MPI_FOUND
    //TODO: OMPI
#endif
#ifdef OPENCL_FOUND
    m_processFactories.insert(std::make_pair(
        OpenCLProcessFactory::tag(),
        std::unique_ptr<AbstractProcessFactory>(new OpenCLProcessFactory())));
#endif
}

Epidemica::DeviceManager::~DeviceManager()
{
}

AbstractProcessFactory* Epidemica::DeviceManager::GetFactory(const std::string& factoryTag)
{
    return m_processFactories[factoryTag].get();
}

ProcessPipeline* Epidemica::DeviceManager::CreateArbovirusProcessPipeline(const std::string& factoryTag)
{
    //List of ordered tags of which implementation to try
    // eg. ["OMPI", "MPI", "OMP", "ST"]
    // Prepends a user defined list to the front
    // eg ["ST", "OMPI", "OMP", "ST"]
    if (!m_processFactories.count(factoryTag))
    {
        throw std::runtime_error("factory not created");
    }

    std::vector<std::string> fallbacktags = { factoryTag };
    for (auto it = Epidemica::Globals::FALLBACKTAGS.cbegin(); it != Epidemica::Globals::FALLBACKTAGS.cend(); it++)
    {
        fallbacktags.push_back(*it);
    }

    std::vector<std::shared_ptr<IProcess>> processes;
    processes.reserve(7);
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateInfectionInVectors(); });
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateInfectionInHosts(); });
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateRegularHumanMovement(); });
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateVectorMovement(); });
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateVectorInfection(); });
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateHostInfection(); });
    TryAddProcess(processes, fallbacktags, [&](std::string tag) { return m_processFactories.at(tag)->CreateVectorPopulation(); });
    return new ProcessPipeline(processes);
}

ProcessPipeline* Epidemica::DeviceManager::CreateYellowfeverProcessPipeline(const std::string& factoryTag)
{
    throw std::runtime_error("yellowfever pipeline not implemented");
}

ProcessPipeline* Epidemica::DeviceManager::CreateZikavirusProcessPipeline(const std::string& factoryTag)
{
    throw std::runtime_error("zikavirus pipeline not implemented");
}

void Epidemica::DeviceManager::TryAddProcess(
    std::vector<std::shared_ptr<IProcess>>& processes, 
    const std::vector<std::string>& fallbacktags, 
    const std::function<IProcess*(std::string)>& factoryMethod)
{
    for (auto fallbackTag : fallbacktags)
    {
        try
        {
            processes.push_back(std::shared_ptr<IProcess>(factoryMethod(fallbackTag)));
            return;
        }
        catch (std::exception&) { /*continue trying*/ }
    }
    throw std::runtime_error("Cannot find process within any factories");
}