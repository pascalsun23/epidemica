#pragma once

#include <memory>
#include "ProgramOptions.h"

namespace Epidemica
{
    class Simulation;
    class DeviceManager;
    class PTreeSimulationScenario;
}

Epidemica::Simulation* LoadSimulation(
    const program_options& options,
    std::shared_ptr<Epidemica::DeviceManager> deviceManager,
    std::shared_ptr<Epidemica::ISimulationScenario> scenario);

void RunSimulation(Epidemica::Simulation& simulation);
void ProcessResults(program_options& vm, Epidemica::Simulation& simulation);
