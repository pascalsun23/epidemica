#pragma once

#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
	class OMPIProcessFactory : public AbstractProcessFactory
	{
	public:
		OMPIProcessFactory();
		~OMPIProcessFactory();
        
        static std::string tag() { return "OMPI"; }
        
        IProcess* CreateProcess(std::string processName) override;
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}