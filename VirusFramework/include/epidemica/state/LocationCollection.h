#pragma once

#include <epidemica/Types.h>
#include <epidemica/state/Location.h>

namespace Epidemica
{
    class LocationCollection
    {
    public:
        vector<Location> Collection;

#ifdef RAPIDJSON_FOUND
        void Save();
#endif
    };
}