#pragma once
#if MPI_FOUND

#include "epidemica/process/AbstractProcessFactory.h"

#include <mpi.h>

namespace Epidemica
{
	class MPIProcessFactory : public AbstractProcessFactory
	{
	public:
		MPIProcessFactory();
		~MPIProcessFactory();
        
        static std::string tag() { return "MPI"; }
        
        ProcessPipeline* CreateProcessPipeline() override;
        IProcess* CreateProcess(std::string processName) override;
        
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;

        virtual void Initialize() override;
        virtual void Finalize() override;

        /**
          * MPI Specific
         **/
        static void InitDatatype();
	};
}
#endif