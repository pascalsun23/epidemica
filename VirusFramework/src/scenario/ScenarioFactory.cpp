#include "stdafx.h"

#include "epidemica/scenario/ScenarioFactory.h"
#include "epidemica/scenario/PTreeSimulationScenario.h"
#include "epidemica/scenario/JSONSimulationScenario.h"
#include "epidemica/state/SimulationState.h"

using namespace Epidemica;

ISimulationScenario* Epidemica::ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(const std::string& filepath)
{
#if RAPIDJSON_FOUND
    return new JSONSimulationScenario(filepath);
#elif BOOST_FOUND
    return new PTreeSimulationScenario(filepath);
#else
    #error "JSON parsing library required"
    return nullptr;
#endif
}

SimulationState* Epidemica::ScenarioFactory::MakeStateFromJSONScenario(const ISimulationScenario& scenario)
{
    return new SimulationState(scenario);
}


