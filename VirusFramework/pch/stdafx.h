#pragma once

///===================
/// STATIC DEFINITIONS
///===================
//These are now set via cmake depending on whethera library is detected,
//but you can override them here if necessary.
//#define OPENMP 1
//#define MPI 1

///=================
/// Common Libraries
///=================
#include <iostream>
#include <chrono>
#include <sstream>
#include <memory>
#include <random>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <set>
#include <valarray>

//============
// Boost
//============
#if BOOST_FOUND
#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/program_arguments.hpp>

#endif

//============
// High Performance
//============
#ifdef OPENMP
#include <omp.h>
#endif

#ifdef MPI
#include <mpi.h>
#endif

//#include <UnitTest++/UnitTest++.h>
//#include <CppUnit.h>