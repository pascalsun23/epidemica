#pragma once

#include <epidemica/process/IProcess.h>
#include <epidemica/state/SimulationState.h>
#include <epidemica/state/VectorState.h>
#include <valarray>
#include <random>

namespace Epidemica
{
    namespace ST
    {
        class STVectorMovement : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            STVectorMovement();
            ~STVectorMovement();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;

            /**
             * returns neighbourId : neighbour's incoming SIER_vector
             */
            std::valarray<seir_vector> DistributeVectors(Location& location, seir_vector movers);
        };
    }
}

