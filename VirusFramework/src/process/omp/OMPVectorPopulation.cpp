#if OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPVectorPopulation.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <valarray>

#include <omp.h>

Epidemica::OMP::OMPVectorPopulation::OMPVectorPopulation()
{
    m_generator = random_engine();
}

Epidemica::OMP::OMPVectorPopulation::~OMPVectorPopulation()
{
}

std::string Epidemica::OMP::OMPVectorPopulation::GetName() const
{
    return "OMP::OMPVectorPopulation";
}

void Epidemica::OMP::OMPVectorPopulation::Execute(SimulationState& state)
{
    float birthRate = state.m_params.vectorBirthRate;
    float deathProbability = 1.0f - std::exp(-state.m_params.vectorDeathRate);
    float matureProbability = 1.0f - std::exp(-state.m_params.vectorMaturationRate);

    #pragma omp parallel for
    for (int k = 0; k < (int)state.m_locations.size(); k++)
    {
        Location& location = state.m_locations[k];

        std::valarray<int> deaths = std::valarray<int>(INFECTION_STATES);
        int vectorPopulation = 0;
        for (int i = 0; i < INFECTION_STATES; i++)
        {
            //TODO: can we precalculate this?
            vectorPopulation += location.m_vectorState.SEIR[i];

            std::binomial_distribution<int> binomial(location.m_vectorState.SEIR[i], deathProbability);
            deaths[i] = binomial(m_generator);
        }

        float birthProbability = (float)vectorPopulation 
            * birthRate
            * std::max(0.0f, 1.0f - vectorPopulation / location.m_vectorState.Capacity);
        
        int births = 0;
        if (birthProbability > 0.0)
        {
            std::poisson_distribution<int> poisson(birthProbability);
            births = poisson(m_generator);
        }

        auto binomial = std::binomial_distribution<int>(location.m_vectorState.Immature, matureProbability);

        int matured = binomial(m_generator);

        //TODO vector subtraction
        //location.m_vectorState.SEIR -= deaths;
        for (int i = 0; i < INFECTION_STATES; i++)
        {
            location.m_vectorState.SEIR[i] -= deaths[i];
        }

        location.m_vectorState.Immature += births;
        location.m_vectorState.Immature -= matured;
        location.m_vectorState.SEIR[InfectionState::S] += matured;
    }
}
#endif