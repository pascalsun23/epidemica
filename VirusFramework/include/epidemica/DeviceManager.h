#pragma once
#include <map>
#include <functional>
#include <memory>
#include <vector>

namespace Epidemica
{
    class IProcess;
    class AbstractProcessFactory;
    class ProcessPipeline;
    
    typedef std::map<std::string, std::unique_ptr<AbstractProcessFactory>> ProcessFactoryMap; 

    /**
     * @brief Manager class for simulation specific device and factory objects.
     *        
     *        The device object that represents the collection of physical hardware available. This 
     *        class contains a dictionary of all the ProcessFactories available at runtime, and can be accessed
     *        via implementation tags (e.g. "ST", "OMP", "OMPI", "MPI", "CL").
     *        
     *        A Factory object is convenience class for dealing with a large number constructors.
     *        Factory classes have a disadvantage however in that they choose where to allocate memory.
     *        (In this case they will always allocate to the heap with the new keyword)
     *
     * ### remarks May also want to parse
     *             a process config file here, which will have an implementation preference
     *             heirarchy, as well as overrides to this for specific processes. This implements a
     *             Singleton design pattern from here:
     *             http://stackoverflow.com/questions/11711920/how-to- implement-multithread-safe-
     *             singleton-in-c11-without-using-mutex.
     * ### remarks this no longer uses a singleton pattern, may want to use it anyway as a quick way
     *             to gain access.
     */
    class DeviceManager
    {
    private:

        ProcessFactoryMap m_processFactories;
        
    public:
        /**
         * @brief Default constructor.
         * ### remarks Probably not the best idea to create all factories at startup, but the
         *     overhead is minimimal and avoids dev confusion about lazy initialization.
         */
        DeviceManager();

        /**
         * @brief Destructor.
         */
        ~DeviceManager();

        /**
         * @brief Gets a factory.
         * @param factoryTag The factory tag.
         * @return Null if it fails, else the factory.
         */
		AbstractProcessFactory* GetFactory(const std::string& factoryTag);

        /**
         * @brief Factory method to create the arbovirus process pipeline.
         * @param factoryTag .
         * @return The new arbovirus process pipeline implementation.
         *
         * ### remarks FactoryMethod is a bit of antipattern of C++, can no longer decide where
         *             memory is allocated. (In this case, heap allocation via "new")
         *             Consider refactoring Factory classes to Devices and pass them into constructors.
         *             It may possibly be beneficial to use memory pool allocation for processes.
         */
        ProcessPipeline* CreateArbovirusProcessPipeline(const std::string& factoryTag);

        /**
        * @brief Factory method to create the yellowfever process pipeline.
        * @param factoryTag .
        * @return The new default process pipeline.
        * @remarks The new yellowfever process pipeline implementation
        */
        ProcessPipeline* CreateYellowfeverProcessPipeline(const std::string& factoryTag);

        /**
        * @brief Factory method to create the arbovirus process pipeline.
        * @param factoryTag .
        * @return The new zikavirus process pipeline.
        */
        ProcessPipeline* CreateZikavirusProcessPipeline(const std::string& factoryTag);

    private:
        /**
         * @brief Try adding a process from each factory in the fallback list
         * @param [in,out] processes     [in,out] If non-null, the processes.
         * @param          fallbacktags  The fallbacktags.
         * @param          factoryMethod The factory method that will fetch the desired process given a factory tag.
         */
        void TryAddProcess(std::vector<std::shared_ptr<IProcess>>& processes, const std::vector<std::string>& fallbacktags, const std::function<IProcess*(std::string)>& factoryMethod);
    };
}