# VirusSimulator

This project is the executable entry point that actually runs simulation.

This a program should accept parameters to create some form of rendering window, possibly including:
* Start/Pause
* Simulation visualisation using sprites
* Simulation realtime analysis and graphing
* Manual intervention of simulation from user
* Tracking and logging of simulation data for post-analysis
* Tracking and logging of performance

Possibly use Core/Actual/Extended product diagram to make it easy to identify
