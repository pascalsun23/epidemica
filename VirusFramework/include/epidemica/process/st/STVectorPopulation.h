#pragma once
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    namespace ST
    {
        class STVectorPopulation : public IProcess
        {
        private:
            random_engine m_generator;
        public:
            /**
             * Initializes a new instance of the <see cref="VectorPopulation"/> class.
             */
            STVectorPopulation();

            /**
             * Finalizes an instance of the <see cref="VectorPopulation"/> class.
             */
            ~STVectorPopulation();
            
            std::string GetName() const override;

            /**
             * Executes the specified state.
             * @param [in,out] state The state.
             */
            void Execute(SimulationState& state) override;
        };
    }
}

