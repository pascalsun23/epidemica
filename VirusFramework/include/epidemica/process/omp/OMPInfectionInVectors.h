#pragma once
#if OPENMP_FOUND
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    struct SimulationState;

    namespace OMP
    {
        class OMPInfectionInVectors : public IProcess
        {
        private:
            random_engine m_generator;

        public:
            OMPInfectionInVectors();
            ~OMPInfectionInVectors();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
#endif
