#include <epidemica/Globals.h>


#include <epidemica/Simulation.h>
#include <epidemica/scenario/PTreeSimulationScenario.h>
#include <epidemica/state/SimulationState.h>
#include <epidemica/DeviceManager.h>
#include <epidemica/process/IProcess.h>
#include <epidemica/process/ProcessPipeline.h>
#include <epidemica/profiling/Stopwatch.h>

#include <chrono>

#include "ProgramOptions.h"

#include "renderer/MatplotlibWrapper.h"


using namespace Epidemica;
using namespace std::chrono;

/**
 * Performs the simulation loading steps with output.
 * @param options       Options for controlling the operation.
 * @param deviceManager Manager for device.
 * @param scenario      The scenario.
 * @return Null if it fails, else the simulation.
 */
Simulation* LoadSimulation(
    const program_options& options,
    std::shared_ptr<DeviceManager> deviceManager,
    std::shared_ptr<ISimulationScenario> scenario)
{
    std::cout << "implementation : " << options["implementation"].as<std::string>() << std::endl;
    std::cout << "Creating simulation..." << std::endl;

    Simulation* simulation = new Simulation(
        options["implementation"].as<std::string>(),
        deviceManager,
        scenario);

    std::cout << "loading processes.." << std::endl;

    simulation->Initialize();
   
    //Must load the scenario into the SimulationState
    simulation->LoadStateFromScenario();

    std::cout << *simulation << std::endl;

    return simulation;
}

/**
 * Executes the simulation and outputs the running time.
 * @param [in,out] simulation The simulation.
 */
void RunSimulation(Simulation& simulation)
{
    //Run
    Epidemica::Stopwatch stopwatch;
    stopwatch.Start();
    while (!simulation.IsDone())
    {
        simulation.Step();
    }
    stopwatch.Stop();

    //Output
    std::cout << "Simulation run time : " << stopwatch.GetTime<double>() << " seconds" << std::endl;
}

/**
 * Process the results in an easily viewable format here. Current supports
 * printstats and epicurve.
 * @param [in,out] options    Options for controlling the operation.
 * @param [in,out] simulation The simulation.
 */
void ProcessResults(program_options& options, Simulation& simulation)
{
    //epidemic curve
    std::vector<seir_vector>& hostsEpidemic = simulation.GetHostsEpidemic();

    if (options.count("printstats"))
    {
        for (int i = 0; i < hostsEpidemic.size(); i++)
        {
            std::cout << "["
                << hostsEpidemic[i][0] << ","
                << hostsEpidemic[i][1] << ","
                << hostsEpidemic[i][2] << ","
                << hostsEpidemic[i][3] << "]" << std::endl;
        }
    }

    if (options.count("profiling"))
    {
        DisplayProfiling(*Globals::GetTimeProfiler());
    }

    //always show epicurve if possible
    if (true || options.count("epicurve"))
    {
#ifdef MATPLOTLIBCPP_PYTHON_HEADER
        DisplayEpidemicCurve(hostsEpidemic);
#else
        //throw std::runtime_error("Curve plotting currently requires Matplotlib-cpp");
#endif
    }

}
