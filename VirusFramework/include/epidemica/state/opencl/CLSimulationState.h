#pragma once
#ifdef OPENCL_FOUND

#include <epidemica/state/SimulationState.h>
#include <CL/cl2.hpp>

namespace Epidemica
{
    namespace OpenCL
    {
        /**
         * @struct SimulationState
         * @author Callan
         * @date 29/03/2017
         * @file SimulationState.h
         * @brief OpenCL implementation needs to alter the simulation state class
         * such that data can be stored in cl buffers, and kept in this state to minimize
         * data copying inbetween processes.
         */
        struct SimulationState : public Epidemica::SimulationState
        {
        private:
            //alternatively, processpipelinec ould handle these.
            cl::Buffer m_hosts;
            cl::Buffer m_locations;
        public:
            SimulationState();
            ~SimulationState();
        };
    }
}
#endif