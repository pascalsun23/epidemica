
#include <epidemica/scenario/JSONSimulationScenario.h>
#include <epidemica/scenario/ScenarioFactory.h>
#include <epidemica/Simulation.h>
#include <epidemica/DeviceManager.h>
#include <epidemica/process/ProcessPipeline.h>
#include <epidemica/state/SimulationState.h>

#include <memory>
#include <gtest/gtest.h>

namespace Epidemica
{
    class ArbovirusSystemTests : public testing::Test
    {
    public:
        std::shared_ptr<DeviceManager> m_deviceManager;
        std::shared_ptr<ProcessPipeline> m_STPipeline;

        ArbovirusSystemTests()
        {
            // You can do set-up work for each test here.
        }

        virtual ~ArbovirusSystemTests()
        {
            // You can do clean-up work that doesn't throw exceptions here.
        }

        // Code here will be called immediately after the constructor (right before each test).
        virtual void SetUp() override
        {
            m_deviceManager.reset(new DeviceManager());
            m_STPipeline.reset(m_deviceManager->CreateArbovirusProcessPipeline("ST"));
        }


        // Code here will be called immediately after each test (right before the destructor). 
        virtual void TearDown() override
        {
            
        }

        /**
         * Test which determines if the simulation scenario loads into state, and can 
         * run for 10 cycles and finish
         * @param filename Filename of the file.
         */
        void RunSystemTest(const std::string& filename)
        {
            std::shared_ptr<ISimulationScenario> scenario =
                std::shared_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::shared_ptr<Simulation> simulation = std::shared_ptr<Simulation>(
                new Simulation("ST", m_deviceManager, scenario, m_STPipeline));

            simulation->Initialize();
            simulation->LoadStateFromScenario();

            simulation->ResetSteps(0, 10);
            simulation->Run();
            ASSERT_EQ(simulation->GetState().m_config.CurrentCycle, 10);

            simulation->SaveStateToScenario();
            
            //TODO save to custom file, then delete
            //simulation->SaveScenarioToCheckpointFile();
            
            simulation->Finalize();
        }

        /**
         * Tests that checkpoints are occuring at the set interval.
         * @param filename Filename of the file.
         */
        void RunCheckpointTest(const std::string& filename)
        {
            std::shared_ptr<ISimulationScenario> scenario =
                std::shared_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::shared_ptr<Simulation> simulation = std::shared_ptr<Simulation>(
                new Simulation("ST", m_deviceManager, scenario, m_STPipeline));

            simulation->Initialize();
            simulation->LoadStateFromScenario();
            simulation->ResetSteps(0, 10);
            simulation->SetCheckpointInterval(3);

            ASSERT_TRUE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_TRUE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_TRUE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_TRUE(simulation->IsCheckpoint());
            simulation->Step();
            ASSERT_FALSE(simulation->IsCheckpoint());
            ASSERT_TRUE(simulation->IsDone());


            simulation->SaveStateToScenario();
            simulation->Finalize();
        }

        /**
         * Executes the loading and saving test operation for JSON scenarios
         * @param filename Filename of the file.
         */
        void RunLoadingAndSavingJSONTest(const std::string& filename)
        {
            //Run a scenario and save
            std::shared_ptr<ISimulationScenario> scenario =
                std::shared_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::shared_ptr<Simulation> simulation = std::shared_ptr<Simulation>(
                new Simulation("ST", m_deviceManager, scenario, m_STPipeline));

            simulation->Initialize();
            simulation->LoadStateFromScenario();
            simulation->SaveStateToScenario();
            simulation->Finalize();

            scenario->SaveScenarioToFile("testsave", "");

            // load the original scenario and saved scenario, there are now 3 different scenarios
            std::unique_ptr<ISimulationScenario> originalScenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::unique_ptr<ISimulationScenario> savedScenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            JSONSimulationScenario& jsonOriginalScenario = static_cast<JSONSimulationScenario&>(*originalScenario);
            JSONSimulationScenario& jsonSavedScenario = static_cast<JSONSimulationScenario&>(*savedScenario);
            JSONSimulationScenario& jsonScenario = static_cast<JSONSimulationScenario&>(*scenario);

            //Compare the original with the final
            ASSERT_TRUE(jsonOriginalScenario.GetParams() == jsonScenario.GetParams());
            ASSERT_EQ(jsonOriginalScenario.GetHosts(), jsonScenario.GetHosts());
            ASSERT_EQ(jsonOriginalScenario.GetLocations(), jsonScenario.GetLocations());
            ASSERT_TRUE(jsonOriginalScenario.Compare(jsonScenario));


            //Compare the backup with the final
            ASSERT_EQ(jsonSavedScenario.GetParams(), jsonScenario.GetParams());
            ASSERT_EQ(jsonSavedScenario.GetHosts(), jsonScenario.GetHosts());
            ASSERT_EQ(jsonSavedScenario.GetLocations(), jsonScenario.GetLocations());
            ASSERT_TRUE(jsonSavedScenario.Compare(jsonScenario));

            //consider removing the testsave file from the bin dir here,
            // use rm on unix, something else on windows, or boost.
        }

        /**
         * Executes the consistency test. This test checks that a deterministic pipeline such
         * as single threaded is running exactly the same for its given seed. This is expected
         * to break easily, but is useful for performing particular optimisations.
         * @param filename       Filename of the file.
         * @param backupfilename The backup file name.
         */
        void RunConsistencyTest(const std::string& filename, const std::string& backupFilename)
        {            
            //Run the scenario and save to backup
            std::shared_ptr<ISimulationScenario> scenario =
            std::shared_ptr<ISimulationScenario>(
                ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::shared_ptr<Simulation> simulation = std::shared_ptr<Simulation>(
                new Simulation("ST", m_deviceManager, scenario, m_STPipeline));


            /*
            Handling Test Failures - When this test fails it means that the simulation is producing
            different outputs for the same seed. Either the scenario changed, or the processes changed.
            If this is intended, uncomment the save line and move this output to the scenario folder.
            */
            simulation->Initialize();
            simulation->LoadStateFromScenario();
            simulation->ResetSteps(0, 1);
            //scenario->SaveScenarioToFile("cycle0", "");
            simulation->Run();
            simulation->SaveStateToScenario();
            //scenario->SaveScenarioToFile("cycle1", "");
            simulation->Finalize();


           std::shared_ptr<ISimulationScenario> backupScenario =
                std::shared_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(backupFilename));

            JSONSimulationScenario& jsonScenario = static_cast<JSONSimulationScenario&>(*scenario);
            JSONSimulationScenario& jsonBackupScenario = static_cast<JSONSimulationScenario&>(*backupScenario);
            
            ASSERT_EQ(jsonBackupScenario.GetParams(), jsonScenario.GetParams());
            ASSERT_EQ(jsonBackupScenario.GetHosts(), jsonScenario.GetHosts());
            ASSERT_EQ(jsonBackupScenario.GetLocations(), jsonScenario.GetLocations());
            ASSERT_TRUE(jsonBackupScenario.Compare(jsonScenario));
        }
    };

    TEST_F(ArbovirusSystemTests, test2x2)                 { RunSystemTest("scenarios/test2x2/config.json"); }
    TEST_F(ArbovirusSystemTests, test2x2_legacy)          { RunSystemTest("scenarios/test2x2legacy/params.json"); }
    TEST_F(ArbovirusSystemTests, test5x4)                 { RunSystemTest("scenarios/test5x4/test.config.json"); }
    TEST_F(ArbovirusSystemTests, test5x4_legacy)          { RunSystemTest("scenarios/test5x4legacy/test.params.json"); }
    
    TEST_F(ArbovirusSystemTests, testcheckpoints)         { RunCheckpointTest("scenarios/test5x4/test.config.json"); }

    TEST_F(ArbovirusSystemTests, test2x2LoadingAndSaving) { RunLoadingAndSavingJSONTest("scenarios/test2x2/config.json"); }
    TEST_F(ArbovirusSystemTests, test5x4LoadingAndSaving) { RunLoadingAndSavingJSONTest("scenarios/test5x4/test.config.json"); }

    TEST_F(ArbovirusSystemTests, test2x2Consistency)
	{
		RunConsistencyTest("scenarios/test2x2/config.json", "scenarios/consistency/test2x2/cycle1.config.json");
	}
}
