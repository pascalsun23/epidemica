cmake_minimum_required(VERSION 3.2 FATAL_ERROR)
include(../cmake/AutoSourceGroup.cmake)
include(../cmake/PrecompiledHeader.cmake)

set(TARGET_NAME HelloWorld)

include_directories(
	.
	pch
	include
	include/epidemica
)

file(
	GLOB_RECURSE CXX_SRCS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.cpp"
)

file(
	GLOB_RECURSE CXX_HEADERS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.h"
)

# Add Project Libraries
#include_directories ("${CMAKE_CURRENT_LIST_DIR}/../VirusFramework/include")
#set(LINK_OPTIONS VirusFramework;${LINK_OPTIONS})

# Add project
add_executable (${TARGET_NAME} ${CXX_HEADERS} ${CXX_SRCS})
target_include_directories(${TARGET_NAME} PUBLIC ${INCLUDE_DIRECTORIES})
target_link_libraries(${TARGET_NAME} ${LINK_OPTIONS})

# add code filters
auto_filter_group("Header Files" ${CXX_HEADERS})
auto_filter_group("Source Files" ${CXX_SRCS})


set_property(TARGET ${TARGET_NAME} PROPERTY FOLDER "executables")

set_target_properties(${TARGET_NAME} PROPERTIES
                      RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
