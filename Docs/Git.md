
# Epidemica - Git

- [Epidemica - Git](#epidemica---git)
	- [Introduction](#introduction)
	- [Tutorial](#tutorial)
	- [Branching Strategy](#branching-strategy)

## Introduction

This project is setup with git version control and has been developed using a git workflow.

## Tutorial

## Branching Strategy

The important concepts in performing a collaboritive C/C++ project are:

- Code should all end up being merged into master once a task is fully complete
- Avoid undoing git commits and performing other workarounds. It's okay to introduce occasional mistakes and use a reverse commit to take it out.
- Use a personal tag or feature name as branch names to make it clear what the branch introduced.
- Do not modify another persons remote branch unless given permission in order to minimize push/pull conflicts.
- Not all branches are equal. Use a heirarchy of branches and use pull requests and reviews to move changes up the heirarchy.

Optionally use a professional branching heirarchy such as this one:

- http://nvie.com/posts/a-successful-git-branching-model/